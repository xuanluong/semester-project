\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Data Set}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Data Set Overview}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Data Anomalies}{5}{0}{2}
\beamer@sectionintoc {3}{Model Construction}{6}{0}{3}
\beamer@subsectionintoc {3}{1}{Logistic Regression}{7}{0}{3}
\beamer@subsectionintoc {3}{2}{Naive Bayesian}{9}{0}{3}
\beamer@sectionintoc {4}{Model Evaluation}{11}{0}{4}
\beamer@subsectionintoc {4}{1}{Experiment Setting}{11}{0}{4}
\beamer@subsectionintoc {4}{2}{Experiment Results}{13}{0}{4}
\beamer@sectionintoc {5}{Model Interpretation}{14}{0}{5}
\beamer@subsectionintoc {5}{1}{Logistic Regression}{14}{0}{5}
\beamer@subsectionintoc {5}{2}{Naive Bayesian}{15}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{16}{0}{6}
