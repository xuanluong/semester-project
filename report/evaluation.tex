\section{Evaluation and Result Interpretation}
\label{sec:experiments}
\subsection{Classification quality evaluation}
\subsubsection{Experiment setting}
We measure the classification quality of the models between learners and visitors in the first \textsf{n} weeks. The ideal situation is \textsf{n}=1, i.e. classify the people who showed in the first week as being learners or visitors. In fact, information from the first week might not be enough since some videos in that week are just introductory and impatient users may ignore them  thus decrease the discrimination power of the data as well. Having a \textsf{n} too high is not desirable though since it would be too late to adapt the course at that time. If the first week is unable to classify users, then the second best option is the first two weeks. Therefore, experiments for \textsf{n}$\in\lbrace 1,2\rbrace$ were conducted to compare their results.

To assess the independence of the results with the data set, we merge the data from both courses. This is reasonable since both courses are both organized by the same institution and instructed (in the first 2 weeks) by the same professor, implicitly imply similar teaching style as well as course organization. Besides, since the processes of estimating the models are supervised, we do $k$-fold cross-validation to avoid the problem of over-fitting the models to a specific data subset. We set $k$=10, split the data into 10 buckets, then select one for testing and the rest for training. The selection of buckets for testing and training is iterated $k$ times so that each bucket is used for testing once. The following measures of classification quality is evaluated at each iteration step: accuracy, Cohen's $\kappa$ %\cite{ckappa}
, and the area under the ROC curve (ROC-AUC). %\cite{roc}.

\begin{figure}[t!]
        \centering
        \begin{subfigure}[b]{0.3\textwidth}
                \includegraphics[width=\textwidth]{fig/accuracy_logit_bayes.pdf}
                \caption{Accuracy}
                \label{subfig:accuracy}
        \end{subfigure}
        \begin{subfigure}[b]{0.3\textwidth}
                \includegraphics[width=\textwidth]{fig/ckappa_logit_bayes.pdf}
                \caption{Cohen's $\kappa$}
                \label{subfig:ckappa}
        \end{subfigure}
        \begin{subfigure}[b]{0.3\textwidth}
                \includegraphics[width=\textwidth]{fig/roc_auc_logit_bayes}
                \caption{ROC - AUC}
                \label{subfig:roc_auc}
        \end{subfigure}
        \caption{Experiment results with respect to 3 classification quality measures: accuracy, Cohen's $\kappa$, and ROC - AUC}
        \label{fig:experiment_results}
\end{figure}
\subsubsection{Experiment results}
Figure \ref{fig:experiment_results} shows the experiment results with respect to the models and the classification quality measures. The crosses and the triangles correspond to the values of the respective measures from each iteration of the validation process. The red (logistic regression) and blue (naive Bayesian) lines connect the means of the measures with respect to \textsf{n}. Figure \ref{fig:experiment_results} reveals that:
\begin{itemize}\itemsep-2pt
	\item Both models performed badly with only information from the first week. The classification quality, however, was improved when the second week was taken into account. For both models, the accuracies were above 0.8, Cohen's kappas are above 0.6, and ROC - AUC are above 0.8. Those are indicators of good classification quality.
	\item The logistic regression classifier generally performed better than the naive Bayesian classifier. However, the performance of logistic regression was inconsistent. Accuracy, Cohen's $\kappa$, and ROC - AUC of logistic regression at different iteration steps of $k$-fold cross-validation spread over a wider range than those of naive Bayesian model.
\end{itemize}
The fact that naive Bayesian model performed more consistently than logistic regression can be explained by its robustness against data anomalies, which was discussed in Section \ref{subsec:bayes}.

\subsection{Result interpretation - logistic regression}
\begin{table}[h]
\centering
\begin{tabular}{lccrc}
  \hline
 & coefficient ($\beta$) & odds ratio ($e^\beta$) & $p$-value & $\sigma$ \\
  \hline
\#minutes playing & 1.26 & 3.54 & 0.00 & 51.40 \\ 
\#pauses at quiz & 0.87 & 2.39 & 0.00 & 10.50 \\ 
\#pauses at slide & 0.24  & 1.27 & 0.00 & 14.34 \\ 
\#jumps back to quiz & -0.17 & 0.84 & 0.00 & 2.07 \\ 
\#jumps back to demo & -0.14 & 0.87 & 0.00 & 1.60 \\ 
\#jumps ahead to demo & 0.09 & 1.10 & 0.05 & 1.45 \\ 
\#jumps ahead to quiz & 0.10 & 1.10 & 0.05 & 2.70 \\ 
   \hline
\end{tabular}
\caption{Coefficients and odds ratio of statistically significant variables}
\label{table:logit}
\end{table}

Table \ref{table:logit} contains the coefficients and the odds ratio with respect to the significant independent variables. We observe that the most significant regressor is the number of minutes playing, i.e. playing more minutes increases the probability of being a learner. Speaking in terms of odds ratio, spending 1 more standard deviation higher than the mean ($\approx$51 minutes) playing videos increases the odds ratio by a factor of 3.54. %Other regressors with positive significant impacts are the number of pauses during a quiz or during a demo, and the number of jumps forward to a demo or a quiz. The significant regressors have negative impacts are the number of jumps back to a quiz or a demo.
Regressors with positive coefficients (or odds ratio greater than 1) have positive impacts while the others have negative impacts.
%Interpreting the coefficients of logistic regressions in terms of odds ratio may be not very intuitive. To grab a sense of the impact of each regressor, we provide a visual illustration in Figure \ref{fig:logit}. 
%The idea is to rewrite the regression equation in Section \ref{subsubsec:logit}:
%\begin{equation}
%	p = \dfrac{1}{1+e^{-(\beta_0 + \beta_1x_1 + \cdots \beta_nx_n)}} = \textsf{logistic}(\beta_0 + \beta_1x_1 + \cdots \beta_nx_n)
%\end{equation}
%The $p$ on the left-hand side is the probability of being a learner. 
%To study the impact of one independent variable $x_i$ on $p$, we project the logistic curve above onto the plane of $p$ and $x_i$. In other words, we plot $y=\textsf{logistic}(\beta_0 + \beta_ix_i)$ on a 2-dimensional plane.
%For each regressor, we fit a single-variate logistic regression model and plot the logistic curve %$y=\textsf{logistic}(\beta_0 + \beta x)$ on a 2-dimensional plane.
\begin{figure}[t]
        \centering
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{fig/logit_coef.pdf}
                \caption{}
                \label{subfig:logit_coef}
        \end{subfigure}
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{fig/logit_odds_ratio.pdf}
                \caption{}
                \label{subfig:logit_odds_ratio}
        \end{subfigure}
        \caption{Confidence intervals of the standard coefficients and odds ratios.}
        \label{fig:logit}
\end{figure}
Figure \ref{fig:logit} depicts the confidence intervals of the coefficients as well as the odds ratio. Variables with negative impacts are colored in red while those with positive impacts are in blue. The further are the confidence intervals (red and blue segments) from the dotted vertical lines%($x=0$ in Figure \ref{subfig:logit_coef} and $x=1$ in Figure \ref{subfig:logit_odds_ratio})
, the greater is its influence on the probability of being a learner, either positive or negative. It can be observed that:
\begin{itemize} \itemsep-2pt
	\item Learners spent more time playing the videos than visitors did. This is reasonable due to the fact that learners also generally watched more videos than visitors.
	\item Learners tend to pause and jump forward more and jump backward less than visitors.
\end{itemize}
One might be tempted to interpret the second observation by saying that pausing more probably means users are taking the videos seriously and spent time to understand a quiz or a slide, jumping forward may be a sign of knowing well the content thus more eager to move faster, while jumping backward may indicate confusion. However, it should be noticed that among these independent variables (pause, jump backward, jump forward), some variables have their coefficients not very far from 0 (except the number of pauses on quiz and on slide). Furthermore, due to the problem of data anomalies presented in Section \ref{subsec:data_anomalies}, it cannot be guaranteed that the coefficients would not be changed dramatically had the loss data been available.

%: increasing curves with two ends asymptotically converge to infinity and a blending part in the middle. From the shapes of the curves, it is observable that the number of pauses during a quiz has the steepest one. The second steepest belongs to the number of pauses during a demo while the number of minutes playing's curve has the most gentle slope. This means one more pause during a quiz increases the probability of being a learner more than during a demo, and much more than having one more minute playing, which agrees with the odds ratios in Table \ref{table:logit}.

%On the other side, Figure \ref{subfig:logit_negative} plots the curves of variables having negative impacts: similar to the curve of positive-impact variables but decreasing. We can observe that the number of jumps back to a quiz has the most gentle-sloped curve. The steeper curves belong to the number of pauses during a slide and the number of jumps back to a demo. This indicates that one more jump back to a demo decreases the probability of being a learner more than one more pause during a slide and much more than one more jump back to a quiz. This again agrees with Table \ref{table:logit}.

%One last thing to be noted is that, in Figure \ref{fig:logit}, the scale on the x-axis varies from a few fifties, to a few hundreds, and even a few five hundreds. It means that the probability of being a learner can only be increased considerably when the corresponding independent variable also goes up or down in a relatively considerable amount with respect to the scale.

\subsection{Result interpretation - naive Bayesian model}
\begin{figure}[h]
\centering
        \begin{subfigure}[b]{0.49\textwidth}
        \centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node[state] (watch)  {\textsf{watch}};
\node[state] (skip) [right of=watch] {\textsf{skip}};
\path (watch) edge  [loop left] node [near end] {\textbf{0.94}} (watch);
\path (skip) edge  [loop right] node [near end] {0.82} (skip);
\path (watch) edge  [bend right] node {0.06} (skip);
\path (skip) edge  [bend right] node {\textbf{0.18}} (watch);
\node (F) [right=1.5cm,above=1.5cm] at (watch) {\textsf{Start}};
\path (F) edge  [left] node {0.51} (watch);
\path (F) edge  [right] node {0.49} (skip);
\end{tikzpicture} \\
Learners
        \end{subfigure}
        \begin{subfigure}[b]{0.49\textwidth}
        \centering
\begin{tikzpicture}[->,>=stealth',shorten >=1pt,auto,node distance=2.8cm,semithick]
\tikzstyle{every state}=[fill=white,draw=black,thick,text=black,scale=1]
\node[state] (watch)  {\textsf{watch}};
\node[state] (skip) [right of=watch] {\textsf{skip}};
\path (watch) edge  [loop left] node [near end] {0.72} (watch);
\path (skip) edge  [loop right] node [near end] {\textbf{0.96}} (skip);
\path (watch) edge  [bend right] node {\textbf{0.28}} (skip);
\path (skip) edge  [bend right] node {0.04} (watch);
\node (F) [right=1.5cm,above=1.5cm] at (watch) {\textsf{Start}};
\path (F) edge  [left] node {0.41} (watch);
\path (F) edge  [right] node {0.59} (skip);
\end{tikzpicture} \\
Visitors
        \end{subfigure}
        \caption{The Markov chains of learners and visitors}
        \label{fig:markov_chains}
\end{figure}
%\begin{table}[ht]
%\centering
%\begin{tabular}{rrrr|rrrr}
%  \hline
% & $\lambda$ & \textsf{watch} & \textsf{skip} & & $\lambda$ & \textsf{watch} & \textsf{skip} \\ 
%  \hline
%\textsf{watch} & 0.51 & 0.94 & 0.06 & \textsf{watch} & 0.41 & 0.72 & 0.28 \\ 
%\textsf{skip} & 0.49 & 0.18 & 0.82  & \textsf{skip} & 0.59 & 0.04 & 0.96 \\ 
%   \hline
%\end{tabular}
%\caption{Initial probabilities and transition probability matrix of learners (left) and visitors (right)}
%\label{tab:mc_parameters}
%\end{table}
Figure \ref{fig:markov_chains} depicts the estimated Markov chains for learners and visitors in their graphical forms. Each node represents a state. The initial probability vector $\lambda$ is represented by the numbers attached to the 2 outgoing arrows from the node \textsf{Start}, while the transition probability matrix $T$ can be extracted from the numbers attached to the blending arrows (self-loops included). It can be observed that:
\begin{itemize} \itemsep-2pt
	\item Visitors are quite likely to skip the first video of week 1 while learners are slightly more likely to watch it. The first video of week 1 is a course introduction video.
	\item The probability of making \textsf{watch}$\rightarrow$\textsf{watch} transition of learners is higher than that of visitors, thus the probability of making \textsf{watch}$\rightarrow$\textsf{skip} is lower. It means learners are more likely to continue to the next video after watching one, while visitors have higher tendency to skip it.
	\item The probability of \textsf{skip}$\rightarrow$\textsf{watch} of learners is higher than that of visitors, thus the probability of \textsf{skip}$\rightarrow$\textsf{skip} is lower. It means learners have higher tendency to come back watching the next video after missing one, while visitors are very likely to keep skipping.
\end{itemize}
%We provide a small example to illustrate the naive Bayesian model. The prior probabilities are estimated as \textsf{Pr}($learner$)=0.52, \textsf{Pr}($visitor$)=0.48. We present 5 representative sequences and compute the posterior probabilities:
%\begin{table}[ht]
%\centering
%\begin{tabular}{rrrrrr}
%\hline
%$s$ & \textsf{Pr}($C$) & \textsf{Pr}($s|C$) & \textsf{Pr}($s|C$)\textsf{Pr}($C$) & \textsf{Pr}($C|%s$) & \\ 
%  \hline
% & {\scriptsize$learner,visitor$} & {\scriptsize$learner,visitor$} & {\scriptsize$learner,visitor$} %& {\scriptsize$learner,visitor$} \\ 
%  \hline
%\textsf{watch}$\rightarrow$\textsf{watch}$\rightarrow$\textsf{watch}$\rightarrow$\textsf{watch} & 0.52 0.48 & 0.42  0.15 & 0.22 0.07 & \textbf{0.75}  0.25 \\ 
%\textsf{skip}$\rightarrow$\textsf{skip}$\rightarrow$\textsf{skip}$\rightarrow$\textsf{skip} & 0.52 0.48  & 0.27 0.52 & 0.14 0.25 & 0.36 \textbf{0.64} \\ 
%\textsf{watch}$\rightarrow$\textsf{skip}$\rightarrow$\textsf{watch}$\rightarrow$\textsf{skip} & 0.52 0.48  & 3e-4 12e-4 & 17e-4 61e-4 & 0.22  \textbf{0.78} \\ 
%\textsf{watch}$\rightarrow$\textsf{watch}$\rightarrow$\textsf{skip}$\rightarrow$\textsf{skip} & 0.52 0.48  & 0.023  0.079 & 0.012 0.038 & 0.24 \textbf{0.76} \\ 
%   \hline
%\end{tabular}
%\end{table}
